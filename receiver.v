// Code your design here
module uart_receiver (clk, reset,rxdata,rxd,led);
//parameter idle = 1'b0;
//parameter receiving = 1'b1;

input clk,reset;
input rxd;
output [7:0] rxdata;
output [7:0] led;
reg shift;
reg  state,nextstate;
reg [3:0] bitcounter;
reg [3:0] samplecounter;
  reg [1:0] inc_samplecounter;  
reg [12:0] counter;
reg [10:0] rxshiftreg;
reg clear_bitcounter,inc_bitcounter,clear_samplecounter;

assign rxdata = rxshiftreg[8:1];
assign led = rxdata;


always @ (posedge clk)
begin
        
 if (reset) begin
    state <= 0;
    bitcounter <= 0;
   counter <= 0;
   samplecounter <= 0;
   rxshiftreg <= 0;
 end

  else begin
       
    if (counter < 3) 
        // 7336
       counter <= counter +1 ;
    else  
      begin
        counter <= 0;
        state <= nextstate;
        if (shift) rxshiftreg <=  {rxd,rxshiftreg[10:1]};
        if (clear_samplecounter) samplecounter <= 0;
         if (inc_samplecounter) samplecounter <= samplecounter + 1;
        if (clear_bitcounter) bitcounter <= 0;
        if (inc_bitcounter) bitcounter <= bitcounter + 1;
     end
   end
end

always @ (state or rxd or bitcounter or samplecounter) 
//  always @ (*)
begin
shift <= 0;
                clear_samplecounter <= 0;
				inc_samplecounter <= 0;
				clear_bitcounter <= 0;
				inc_bitcounter <= 0;
 
  
//nextstate <= 0;
 
case (state)
 0 : begin
            if (rxd)
              begin
              nextstate <= 0;
              
              end
           else  begin
               nextstate <= 1;
               clear_bitcounter <= 1;
                clear_samplecounter <= 1;
            end
         end
  1 : begin

   // if (samplecounter == 1) 
     //         begin
               shift <= 1;
                 
                //if (samplecounter == 3) 
                 //begin 
                   if (bitcounter == 10) 
                   begin
                         nextstate <= 0;
                   end

                  inc_bitcounter <= 1;
                 // clear_samplecounter <= 1;
                 //end 
               
             // else
               // begin
                // inc_samplecounter <= 1;
               // clear_bitcounter <= 0;
                //clear_samplecounter <= 0;
                //end
        //    end // upper if loop   
              end 
    
endcase

    end
endmodule             


 