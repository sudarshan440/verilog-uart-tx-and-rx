module test;
  reg clk,reset,rxd,transmit;
  reg [7:0] data;
  wire TxD;
  wire [7:0] rxdata;
  
  transmitter tx (
  .clk(clk),
.reset (reset),
.transmit(transmit),
.data(data),
.TxD(TxD)
);

  uart_receiver rx (
  .clk(clk),
.reset (reset),
.rxdata(rxdata),
.led(led),
.rxd(rxd)
);

  initial 
begin
  $dumpfile("dump.vcd");
  $dumpvars(0,test);

 clk = 0;
reset = 1;
rxd = 1;
transmit = 0;
data = 0;
#500 transmit = 0;


end //initial

initial
begin
  #50 reset = 0;
  #10 data = 8'b01001101;
#15 transmit = 1;

#100 rxd = 1'b0;
  
  #100 rxd = 1'b1;
#100 rxd = 1'b0;
#100 rxd = 1'b1;
#100 rxd = 1'b0;
#100 rxd = 1'b1;
#100 rxd = 1'b0;
#100 rxd = 1'b1;
#150 rxd = 1;
#10 rxd = 0;  


end 
  
always #20 clk = ~clk;

endmodule 
  
  
 